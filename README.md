# Description
**ICON-bootstrap** is a set of scripts and configuration files that help
[ICON](https://code.mpimet.mpg.de/projects/iconpublic) users and developers to
install software dependencies of the model with the package manager
[Spack](https://spack.io).

The main objective of this project is to adjust Spack to the needs of the ICON
community. Although the package manager is flexible enough to works out of the
box in many cases, especially in common Linux/Unix desktop environments, it
often requires additional configuration in the cases of supercompting
environments, mainly due to the deep level of their customization. This project
aims to provide [Spack configuration files](http://spack.readthedocs.io/en/latest/configuration.html)
for a [particular set](#supported-configurations) of software environments that
are most commonly used by the community.

The project also contains a
[repository](https://spack.readthedocs.io/en/latest/repositories.html) of [Spack
packages](https://spack.readthedocs.io/en/latest/workflows.html#package-concrete-spec-installed-package)
that extends the [built-in repository](https://github.com/spack/spack/tree/develop/var/spack/repos/builtin)
of Spack by providing the installation recipies for (mainly) non-public software
required by the model.

# Table of contents
1. [Basic usage](#basic-usage)
    - [Hints](#hints)
2. [Technical details](#technical-details)
    - [Implementation details](#implementation-details)
    - [Known issues and limitations](#known-issues-and-limitations)
3. [Supported configurations](#supported-configurations)
4. [Tested configurations](#tested-configurations)

# Basic usage
In order to install and configure Spack, you need to run the following
commands:
```console
$ git clone https://gitlab.dkrz.de/m300488/icon-bootstrap.git
$ cd ./icon-bootstrap
$ ./bootstrap
$ . ./spack/share/spack/setup-env.sh
```

_**Note:** by default, [./bootstrap](bootstrap) installs Spack to the
subdirectory `./spack` and configures it to install the software to the
subdirectory `./opt` inside the current working directory._

Having done that, you can start using Spack following the
[documentation](https://spack.readthedocs.io/en/latest/basic_usage.html).
The following command installs all the software dependencies of ICON:

```console
$ spack install icon-devel
```
_**Note:** depending on the software environment you work in and the compiler
you want to use, the actual installation command might differ (see
[hints](#hints) and [tested configurations](#tested-configurations))._


_**Note:** you can use Spack for installations of other packages that you need
(e.g. `spack install cdo`) but keep in mind that the configuration performed by
[./bootstrap](bootstrap) might have package and compiler preferences different
from Spack's default ones, including, for example, the usage of system versions
of `python` or `perl` (in order to decrease the installation time of the
`icon-devel` package) that are installed to `/usr` directory, which will not
let you to [activate](https://spack.readthedocs.io/en/latest/packaging_guide.html?highlight=extension#activation-deactivation)
the [extensions](https://spack.readthedocs.io/en/latest/packaging_guide.html?highlight=extension#extensions)
(e.g. [py-netcdf](http://unidata.github.io/netcdf4-python)) because this would
require root privileges. A solution to this is to override the configuration by
creating a file `~/.spack/packages.yaml` (for more details refer to the
[build customization](https://spack.readthedocs.io/en/latest/build_settings.html#build-customization])
section of Spack documentation)._

## Hints
1.  You will likely to experience problems if you run Spack from a shell
    interpreter that is not a version of `bash`. It is recommended to switch
    to bash before you run [./bootstrap](bootstrap) and Spack using the
    following command (login mode is required to get a properly initialized
    shell, e.g. the shell command `module`):

    ```console
    $ bash --login
    ```
2.  Spack can't inject the correct PIC flag for the combination of GCC+NAG but
    the configuration scripts of zlib and hdf5 know the flags for GCC. So, build
    icon-devel with NAG using the following command:

    ```console
    $ spack install icon-devel%nag ^zlib~pic ^hdf5~pic
    ```
3.  It is recommended to instruct Spack to build the utilities that are required
    only for building of other packages with GCC when the main compiler is
    neither GCC nor Intel:

    ```console
    $ spack install icon-devel%cce ^cmake%gcc ^libtool%gcc ^automake%gcc ^autoconf%gcc ^perl%gcc ^python%gcc ^pkgconf%gcc
    ```
4.  Cray compiler can't build libxml2, so build it with GCC (combine this with
    the previous command):

    ```console
    $ spack install icon-devel%cce ^libxml2%gcc
    ```
5.  You can build for a particular processor architecture if you are on a Cray
    machine by specifying the target on the command line:

    ```console
    $ spack install icon-devel target=ivybridge
    ```
    You can run the following command in order to get a rough list of available
    targets (looks like Spack is missing a feature):
    
    ```console
    $ module avail -t 2>&1 | grep -Po '^craype-(?!hugepages|network|target|accel|xtpe)(\S*)' | cut -d- -f2-
    ```
    Currently, Spack uses a similar logic to find the environment modules that
    set compilation targets for Cray compiler wrappers (i.e. `cc`, `CC`, and
    `ftn`). If the target is not specified on the command line, Spack will
    compile for the target that is set by the module that is loaded at the
    beginning of the login session (Spack runs a separate login shell for
    this, which means that the default target cannot be changed by switching the
    module right before running Spack). The following command prints the default
    target:
    
    ```console
    $ spack arch --target
    ```
    The default compilation target on a Cray machine usually corresponds with
    compute nodes. If the processors of the login nodes (where you are expected
    to run Spack) and the compute nodes are different, you might experience
    problems with packages that either run compiled executables at the building
    stage (e.g. `hdf5`) or support the building stage of other packages (e.g.
    `pkgconf`). A solution to this is to instruct Spack to build the problematic
    packages for the target the corresponds with the login nodes. For example,
    if the login nodes run on `sandybridge` processors, and the compute nodes
    run on `haswell` processors, you can try the following command:
    
    ```console
    $ spack install icon-devel target=haswell ^hdf5 target=sandybridge ^cmake target=sandybridge ^pkgconf target=sandybridge
    ```
    See section [cross compilation](#cross-compilation) for more details.
6.  The testing suite of ICON relies on the reproducibility of the results,
    which can be affected by a hight optimization level of `blas` and `lapack`
    libraries. To disable the optimization of the libraries, you can run the
    following commad:

    ```console
    $ spack install icon-devel ^netlib-lapack@3.4.2 build_type=Debug
    ```
7.  Both **ICON-bootstrap** and Spack require the Internet connection. The
    former needs it to download Spack, the latter uses the connection to
    download the tarballs of the packages. Thus, you might need to perform a
    _remote bootstrapping_ if you need to install packages on a machine that
    cannot establish outgoing connections to the Internet. The procedure
    consists of the following basic steps:
    - upload the repository snapshots of
      [**ICON-bootstrap**](https://gitlab.dkrz.de/m300488/icon-bootstrap/-/archive/master/icon-bootstrap-master.tar.gz)
      and [Spack](https://github.com/spack/spack/archive/develop.tar.gz) to the
      remote machine (e.g., by using a secure shell) and deploy Spack there by
      calling [./bootstrap](bootstrap);
    - use the _same_ snapshots on a computer that has a connection to the
      Internet to deploy Spack locally;
    - run the local version of Spack to create a [package mirror](https://spack.readthedocs.io/en/latest/mirrors.html)
      containing all the dependencies for `icon-devel`;
    - upload the package mirror to the remote machine and configure the remote
      instance of Spack to take the package tarballs from that mirror.
   
    Having done that, you can start the installation of the packages on the
    remote machine as usually. There are a several pitfalls to have in mind when
    performing the described steps. The main problem is that you need the local
    and the remote instances of Spack to be synchronized in terms of their
    versions and the contents of their configuration files
    [`packages.yaml`](https://spack.readthedocs.io/en/latest/build_settings.html#build-settings).
    Both of these parameters are site-specific (see section on
    [installation and configuration of Spack](#installation-and-configuration-of-spack)).
    You can find an example of how the required synchronization can be
    implemented in the file [./remote.example](remote.example).

# Technical details
This section is mainly written for the potential contributors of the project but
it also might help the users to understand the limitations of the implemented
solutions and the differences between the vanila Spack and the one installed
with [./bootstrap](bootstrap).

## Implementation details

We do not intend to tweak the source code of Spack in any way. If there is an
issue, a bug or a feature that we need to resolve, to fix or implement, we
submit them directly to its [developers](https://github.com/spack/spack). The
only thing that we plan to do is to use the existing features of Spack in
order to adjust it to our needs.

The above, up to some extent (see the
[section](#internal-repository-of-spack-packages) below), applies to the
packages (i.e. the building recipies) too. Our default strategy for any new
package or any update of a package that we need is to get it merged to the
[built-in repository of Spack](https://github.com/spack/spack/tree/develop/var/spack/repos/builtin)
because it potentially increases the number of its users, hence providing better
testing and support.

### Installation and configuration of Spack
One of the features of Spack that we rely on is called
[configuration scopes](https://spack.readthedocs.io/en/latest/configuration.html#configuration-scopes).
This feature helps us to have a maintainable set of a **common** and
**site-specific** (specific for a particular platform, site or machine)
configurations. [Files](https://spack.readthedocs.io/en/latest/configuration.html#configuration-files-in-spack)
of the **common** configuration reside in [./config/common](config/common) and
are meant to extend/override the _default_ configuration ("factory" settings) of
Spack. Files of the **common** configuration are unconditionally put to the
_site_ scope of Spack. Files of a **site-specific** configuration are stored in
[./config/sites](config/sites) and, depending on the value of the variable
`SPACK_SITE_CONFIG` (see below), are put to the _site/\<platform\>_ scope in
order to extend/override the **common** configuration in a particular
environment.

Our experience shows that some of the configuration files need to be generated
at runtime. For example, the directory where Spack will install the software,
needs to be specified as a full path in the `config.yaml` file. If we want the
package manager to perform installations to the `./opt` directory inside the
current working directory, we need to generate the file when running
[./bootstrap](bootstrap). To allow for that, each set of configuration files
(the **common** or a **site-specific**) can have an executable names
`init.config` in its directory. If the executable exists, it will be run before
applying any site-specific configuration file. The executable must follow the
following interface (see
[./config/common/init.config](config/common/init.config)):
 - it must be possible to run the executable from an arbitrary working
   directory;
 - the executable should avoid emitting any messages to the `stdout` or
   `stderr`;
 - the executable should not be interactive (i.e. should not use any input from
   `stdin`);
 - the exit status of the executable should be zero on success and non-zero
   otherwise;
 - all the configuration files generated by the executable must be saved to the
   same directory where it resides;
 - the executable can rely on the fact that it resides in
   `./config/sites/<configuration-name>`.

_**Note:** it is strongly recommended to put the automatically generated files
to `./config/sites/<configuration-name>/.gitignore`._

The installation and configuration of Spack is performed by the
[./bootstrap](bootstrap) shell script in the following steps:
1. The script _sources_ the file [./config/shell.detect](config/shell.detect),
   which runs a sequence of tests in order to identify the dialect (`bash`,
   `zsh`, `ksh`, `csh`, or `dash`) of the current shell interpreter. If the
   shell is not a version of `bash` or `zsh`, which are most supported by Spack,
   the script emits a warning message recommending the user to switch to `bash`
   (see [hints](#hints)). The result of the test also triggers the content of
   the message on how to initialize the shell to enable Spack, which is printed
   at the last step of the script.
2. The script checks whether the user has the passwordless access to the ICON
   repository (git@git.mpimet.mpg.de:icon.git) and emits a warning message if
   it is not the case. It is implied that if the user has a configured access to
   that repository, than she has access to the repositories of other internal
   packages hosted at git.mpimet.mpg.de (e.g. `libmtime`). This step is supposed
   to help the users not to get confused by the error messages emited by Spack
   when installing the packages that it cannot download.
3. The script _sources_ the file [./config/site.config](config/site.config),
   which runs a sequence of tests (mainly based on the `uname` and `host`
   utilities) in order to identify if an additional **site-specific**
   configuration needs to be applied. Site maintainers (i.e. those interested
   in/responsible for supporting a particular platform, site or machine) are
   expected to implement a rule that would help the script to identify their
   site and, depending on the result of the check, override the following
   variables:
   - `SPACK_SITE_CONFIG`: name of a directory inside
     [./config/sites](config/sites) that contains **site-specific**
     configuration that needs to be applied.
   - `SPACK_VERSION`: branch/tag/commit in
     [Spack repository](https://github.com/spack/spack) that must be installed
     for their site. By default [./bootstrap](bootstrap) installs the `HEAD` of
     the [develop](https://github.com/spack/spack/tree/develop) branch since
     this branch is the first one to get the updates (including new versions of
     the built-in packages), and at the same time the developers of Spack try to
     keep it tested and functional after every commit. Nonetheless, it might
     happen that the branch gets a bug, which we can workaround by installing
     the last known working commit or version of Spack for a particular site or
     for all of them.
   - `SPACK_DETECT_COMPILERS`: a `yes`/`no` (default `yes`) variable that
     triggers Spack to run the
     [compiler detection command](https://spack.readthedocs.io/en/latest/getting_started.html#compiler-configuration)
     as the last step of the bootstrapping stage. Most of the
     [supported configurations](#supported-configurations) are provided with
     `compilers.yaml` file that enable Spack to use the available compilers.
     Running the compiler detection command for them might not only be redundant
     but also lead to the detection of a compiler that is known not to be
     working, which potentially confuses the users. At the same time, running
     the detection command is the only way in general to provide the user with a
     usable configuration.
4. The script downloads the snapshot of the `SPACK_VERSION` and unpacks it to
   the `./spack` directory (can be overriden by the command-line argument
   `--spack-root`) inside the current working directory. This step requires
   `curl` and `tar` (GNU and BSD versions of the tools should work) to be
   already available on the system.
5. The script applies the **common** configuration.
6. The script applies the **site-specific** configuration based on the value of
   `SPACK_SITE_CONFIG`.
7. Depending on the value of the `SPACK_DETECT_COMPILERS` variable, the script
   runs the the [compiler detection command](https://spack.readthedocs.io/en/latest/getting_started.html#compiler-configuration):
   `spack compiler find --scope=site/<platform>`.
8. The script emits a short message informing the user on how to proceed.

### Internal repository of Spack packages
The second feature of Spack that we heavily rely on is the
[hierarchy of package repositories](https://spack.readthedocs.io/en/latest/repositories.html).
This feature allows us to extend and override the set of the
[built-in installation recipes and patches](https://github.com/spack/spack/tree/develop/var/spack/repos/builtin)
that come with Spack. As it was mentioned earlier, we try to add all the
packages that we need to the built-in repository. Unfortunately, it is not
always acceptable for the following reasons:
1. The model itself and some of the libraries it requires are distributed under
   license agreements that limit access to their source code, which makes the
   package scripts unusable by external users.
2. Some of the libraries required by the model are either too specific or not
   mature enough to justify their appearence in the built-in repository.
3. It often takes time for the maintainers of Spack to merge the updates. A
   separate repository brings more independence from such delays.

Among all the packages in the [built-in](https://github.com/spack/spack/tree/develop/var/spack/repos/builtin)
and our [internal](repo/packages) repositories, the package
[icon-devel](repo/packages/icon-devel/package.py) has a special role. It does
not install anything but just contains a list of packages required to build ICON
model. We plan to replace this package with a fully functional one, so the
users, who don't need to change the source code of the model, would be able to
get its executables as easy as executables of any other package. The developers
of the model will be able to achieve the current behaviour by running
`spack install --only=dependencies icon`.

## Known issues and limitations
Spack has its own list of [known issues](https://spack.readthedocs.io/en/latest/known_issues.html?highlight=known%20issues).
This section provides additional information on the implementation details of
Spack and **ICON-bootstrap**, which can lead to unexpected behaviour in some
cases.

### Dynamic linking
Although Spack supports static linking in general, many of its package
installation recipes are implemented under the assumption that their
dependencies are linked dynamically. For example, the recipe of a package
depending on `lapack` might instruct Spack to pass only the flag `-llapack` to
the linker assuming that it will be linking to the dynamic library
`liblapack.so` that have the required attributes to locate the library that
implements `blas`, which is a mandatory dependency for `lapack`. Such
implementation would also rely on the fact that Spack injects the required
attribute `DT_RPATH` into all shared libraries and executables it builds by
calling the linker with the flag `-rpath` pointing to the installation
directories of all the dependencies of the package.

The advantages of the dynamic linking made it the default type of linking in the
vast majority of Linux environments, which lead to the situation, when many
packages are well-tested for dynamic linking but lack the proper support for
static linking. Since dynamic linking is what the users expect in most of the
cases and that Spack provides support for it by injecting the `DT_RPATH`
attributes, the lack of support for static linking is usually not considered as
a big problem. Nonetheless, the issue is recognized and Spack tries to enforce
dynamic linking in the environments that are known to perform static linking by
default. For example, Spack sets the environment variable
`CRAYPE_LINK_TYPE=dynamic` on Cray machines.

The default behaviour of the linker is not to inject new dynamic tags
(see the description of the option `--enable-new-dtags` in
[`man 1 ld`](http://man7.org/linux/man-pages/man1/ld.1.html)). This is not the case
for some systems though, meaning that in those environments, the linker
processes the argument `-rpath` in a different way: it either injects the
attribute `DT_RPATH` together with the attribute `DT_RUNPATH` or injects only
the latter. In both cases, this affects the way the dynamic loader locates
secondary dependencies (i.e. the dependencies of the immediate dependencies)
of a library or an executable (see [`man 8 ld.so`](http://man7.org/linux/man-pages/man8/ld.so.8.html)).
This will not be a problem if all dependencies are built with Spack because
each of them will have `DT_RPATH` or/and `DT_RUNPATH` pointing to all
_their_ immediate dependencies plus the additional directories provided as
`extra_rpaths` in the configuration file `compiler.yaml` (see
[Spack documentation](https://spack.readthedocs.io/en/latest/basic_usage.html?highlight=extra_rpaths#compiler-environment-variables-and-additional-rpaths)).
However, the users often want or have to use packages that are already
installed by system administrators. This is especially relevant for MPI
libraries, which are very hardware-specific, meaning that the very generally
configured MPI implementations installed with Spack simply will not work. Spack
supports such cases by allowing for specification of
[external packages](https://spack.readthedocs.io/en/latest/tutorial_configuration.html#external-packages).
If such a package is required for building of another one, Spack will not build
it (or, depending on the configuration, will try to avoid doing so) but use the
version that is already available in the environment. In the context of the
described problem, this means that the chain of `DT_RUNPATH` attributes breaks
if an external package does not have one. For example, Cray's implementation of
MPI library depends on `libudreg` and `libxpmem` but does not have information
on how to find those (neither `DT_RPATH` nor `DT_RUNPATH` is set). Setting the
corresponding value to the environment variable `LD_LIBRARY_PATH` in
`compilers.yaml` can help to get a dependent package built but its following
usage will be complicated by the need to set `LD_LIBRARY_PATH` in advance. To
avoid this, the maintainers are strongly recommended to make sure the the
dynamic loader on their site is configured in a way that allow it to locate such
libraries without the modification of `LD_LIBRARY_PATH`. A good solution to this
is to apply corresponding modifications to the contents of `/etc/ld.so.conf`.

There are two options for enabling Spack to handle this problem without
delegating it to system administrators:
1. Implement a more complex logic for passing flags to the linker. In this
   case, we would be able to set `ldfalgs` in `compilers.yaml` to
   `--disable-new-dtags` to enforce the old way of treating the argument
   `-rpath`. Unfortunately, Spack currently does not support different linking
   flags for the case, when linking is performed through a compiler (the flag
   has to be passed with a prefix: `-Wl,--disable-new-dtags`), and for the case,
   when the linker is called directly.
2. To make the old way of treating the argument `-rpath` the default and the
   unconditional one. This means that Spack's compiler wrapper would filter out
   all arguments that affect `DT_RPATH` (e.g. `--enable-new-dtags`) and include
   addirional argument `--disable-new-dtags`.

The second option, especially since it enforces the behaviour of the linker that
Spack relies on, seems more preferable at the moment. The authors of this
project will try to make sure that it will be implemented in Spack's main
repository.

### Cross compilation
Spack has a limited support for cross compilation. The default architecture
Spack builds for can be retrieved by running the following command:
```console
$ spack arch
```
The users can specify the
architecture of their choice on the command line:
```console
$ spack install icon-devel arch=linux-debian8-x86_64
```
The value of the parameter `arch` is a triplet:
`<platform>-<operating system>-<target>`. The latter two elements of the triplet
can be also specified individually:
```console
$ spack install icon-devel os=debian8 target=x86_64
```
The values for `os` and `target` correspond to the fields `operating_system` and
`target` specified for each compiler in the configuration file `compilers.yaml`.
Based on the values on the command line, Spack filters the compilers that can be
used for compilation, chooses one of them to be used for building (accounting
for other constraints, e.g. the compiler vendor constraint `%intel` on the
command line), and sets up the building environment in accordance to other
configuration parameters of the compiler (e.g. `environment` and `modules`,
see section [Advanced Compiler Configuration](https://spack.readthedocs.io/en/latest/tutorial_configuration.html#advanced-compiler-configuration)
in Spack documentation).
The rest of the cross compilation logic must be implemented either in Spack
package recipes, which is rarely done, or in the configuration scripts (i.e. the
scripts provided by the developers of the software along with the source code,
which are usually implemented with Autotools or CMake) of the packages. Ideally,
a configuration script should not try to run (cross) compiled executables or at
least it should not fail the whole configuration stage when being unable to do
so. The software developers are usually aware of this problem and follow this
recommendation. Unfortunately, it is not always possible. For example, a
configuration script based on Autoconf (part of Autotools) would unconditionally
compile, link, and run a _simple_ program in order to check whether the cross
compilation takes place, and if it does, the configuration script would fail
reporting that the user needs to specify the argument `--host` if we mean to
cross compile. The value for the argument usually must be recognized by the
version of the shell script [config.sub](http://git.savannah.gnu.org/gitweb/?p=config.git;f=config.guess;hb=HEAD)
provided in the release tarball of the package). Spack does not do this by
default because the logic triggered by the value of this argument is
package-specific and often leads to unexpected and unwanted results.

The _simple_ programs that are used for the cross compilation check are the
following:
1. The C program in the original formatting:

    ```c
    #include <stdio.h>
    int
    main ()
    {
    FILE *f = fopen ("conftest.out", "w");
     return ferror (f) || fclose (f) != 0;
    
      ;
      return 0;
    }
    ```
2. The Fortran program in the original formatting:
    ```fortran
         program main
         open(unit=9,file='conftest.out')
         close(unit=9)
    
         end
    ```

Luckily, the programs **are** _simple_, which leads to false negative results of
the cross compilation checks if the processors of the _build machine_ (e.g. a
login node of a cluster) and the _target machine_ (e.g. a compute node of a
cluster) are similar, thus making it easy to overcome the described, often
artificial and redundant, constraint. For example, such situation is common
for Cray machines, where we can perform implicit cross compilations of most of
the required packages.

Nonetheless, some of the required packages (i.e. `hdf5`) need to run (cross)
compiled executables at the building stage. Again, if the processors of the
_build machine_ and the _target machine_ are not significantly different,
meaning, in this case, that the _target machine_ can run the code compiled for
the _build machine_, the users can instruct Spack to build a particular package
for the architecture of the _build machine_ using the following syntax:

```console
$ spack install icon-devel target=haswell ^hdf5 target=sandybridge
```

Another possible solution available at the user level is to avoid the cross
compilation at all by running Spack on a compute node (not tested yet).

Another solution, which is usually available only at the level of a system
administrator (i.e. requires root privileges), is to
[register the binary format](https://www.kernel.org/doc/html/latest/admin-guide/binfmt-misc.html)
of the executables that must be run on the _target machine_ when executed on the
_build machine_. This would allow for completely transparent cross compilation.

### Whitespaces in the paths
Spack cannot treat paths containing whitespaces properly:
1. Spack is unable to call its compiler wrapper if the path to its installation
   directory (i.e. the directory where Spack itself is installed) contains
   whitespaces. The path to the Spack installation directory can be specified
   as the command-line argument `--spack-root` of the
   script [./bootstrap](bootstrap).
2. Spack is unable to install packages to directories containing whitespaces in
   their paths. By default (can be overriden by a site-specific `config.yaml`,
   see the section on [installation and configuration](#installation-and-configuration-of-spack)),
   the script [./bootstrap](bootstrap) configures Spack to install packages to a
   subdirectory (`./opt`) of the current working directory.

Thereby, although these issues can be potentially worked around by experienced
users, the script [./bootstrap](bootstrap) explicitly fails if either the
path to the current working directory or the path to the Spack installation
directory contains whitespaces.

# Supported configurations
Although Spack is flexible enough to works in many different Linux/Unix
environments we put extra efforts to support the following environments.

| Platform/Site/Machine | Status | Current maintainers |
| :---: | :---: | :---: | :---: | :--- |
| Debian 8 ("jessie") desktops at [MPI-M](https://mpimet.mpg.de/startseite) | [Test results](#standard-debian-8-jessie-desktops-mpi-m-mpim-jessie) | Sergey Kosukhin (@m300488) |
| Supercomputer [Mistral at DKRZ](https://www.dkrz.de/up/systems/mistral) | [Test results](#mistral-dkrz-dkrz-mistral) | Sergey Kosukhin (@m300488) |
| Supercomputer XCE at [DWD](https://www.dwd.de/DE/Home/home_node.html) | [Test results](#xce-dwd-xce-dwd) | Sergey Kosukhin (@m300488) |
| Supercomputer [Piz Daint at CSCS](https://www.cscs.ch/computers/piz-daint) | [Test results](#piz-daint-cscs-cscs-daint) | Sergey Kosukhin (@m300488) |
| Mac OS | [Test results](#mac-os-darwin) | Sergey Kosukhin (@m300488) |

# Tested configurations

ICON-bootstrap is supposed to run in all environments supported by Spack.
Nonetheless, we put additional efforts into support of the following
environments.

## Standard Debian 8 ("jessie") desktops @MPI-M (*mpim-jessie*)

| Compiler | Status | Command | Date | Tested by | Comment |
| :---: | :---: | :---: | :---: | :---: | :--- |
| GCC 7.2.0 | ![OK](https://placehold.it/15/00ff00/000000?text=+) | `spack install icon-devel` | 2018-08-03 | Sergey Kosukhin (@m300488) | |
| Intel 17.0.1 | ![OK](https://placehold.it/15/00ff00/000000?text=+) | `spack install icon-devel%intel` | 2018-08-03 | Sergey Kosukhin (@m300488) | |
| NAG 6.2 | ![OK](https://placehold.it/15/00ff00/000000?text=+) | `spack spec icon-devel%nag ^zlib~pic ^hdf5~pic` | 2018-08-03 | Sergey Kosukhin (@m300488) | |
| PGI 15.4 | ![ERROR](https://placehold.it/15/ff0000/000000?text=+) | `spack install icon-devel%pgi ^cmake%gcc ^libtool%gcc ^automake%gcc ^autoconf%gcc ^perl%gcc ^python%gcc ^pkgconf%gcc` | 2018-08-03 | Sergey Kosukhin (@m300488) | Fails to build `libmtime` due to a problem with C compiler. |
 
## Mistral @DKRZ (*dkrz-mistral*)

| Compiler | Status | Command | Date | Tested by | Comment |
| :---: | :---: | :---: | :---: | :---: | :--- |
| GCC 7.1.0 | ![OK](https://placehold.it/15/00ff00/000000?text=+) | `spack install icon-devel` | 2018-08-06 | Sergey Kosukhin (@m300488) | |
| Intel 18.0.2 | ![OK](https://placehold.it/15/00ff00/000000?text=+) | `spack install icon-devel%intel` | 2018-08-06 | Sergey Kosukhin (@m300488) | |
| NAG 6.2 | ![OK](https://placehold.it/15/00ff00/000000?text=+) | `spack spec icon-devel%nag ^zlib~pic ^hdf5~pic` | 2018-08-06 | Sergey Kosukhin (@m300488) | |
| PGI 17.7 | ![ERROR](https://placehold.it/15/ff0000/000000?text=+) | `spack install icon-devel%pgi ^cmake%gcc ^libtool%gcc ^automake%gcc ^autoconf%gcc ^perl%gcc ^python%gcc ^pkgconf%gcc` | 2018-08-06 | Sergey Kosukhin (@m300488) | Failes to build `mpich` because its configuration script is confused with the combination of `FC` and `F90` variables set by the environment module. |

## Piz Daint @CSCS (*cscs-daint*)

| Compiler | Status | Command | Date | Tested by | Comment |
| :---: | :---: | :---: | :---: | :---: | :--- |
| GCC 7.3.0 | ![OK](https://placehold.it/15/00ff00/000000?text=+) | `spack install icon-devel` | 2018-08-07 | Sergey Kosukhin (@m300488) | |
| Intel 17.0.4.196 | ![OK](https://placehold.it/15/00ff00/000000?text=+) | `spack install icon-devel%intel` | 2018-08-07 | Sergey Kosukhin (@m300488) | |
| Cray 8.7.0 | ![ERROR](https://placehold.it/15/ff0000/000000?text=+) | `spack install icon-devel%cce ^cmake%gcc ^libtool%gcc ^automake%gcc ^autoconf%gcc ^libxml2%gcc` | 2018-08-07 | Sergey Kosukhin (@m300488) | Failes to build `libcdi` due to a bug in C compiler. |

## XCE @DWD (*xce-dwd*)

| Compiler | Status | Command | Date | Tested by | Comment |
| :---: | :---: | :---: | :---: | :---: | :--- |
| GCC 7.3.0 | ![OK](https://placehold.it/15/00ff00/000000?text=+) | `spack install icon-devel ^hdf5 target=sandybridge ^cmake target=sandybridge ^pkgconf target=sandybridge` | 2018-08-16 | Sergey Kosukhin (@m300488) | See the relevant issue with [dynamic linking](#dynamic-linking) |

## Mac OS (*darwin*)

| Compiler | Status | Command | Date | Tested by | Comment |
| :---: | :---: | :---: | :---: | :---: | :--- |
| GCC 6.4.0 | ![OK](https://placehold.it/15/00ff00/000000?text=+) | `spack install icon-devel%gcc ^cmake%clang` | 2018-08-06 | Sergey Kosukhin (@m300488) | Compiler installed with [MacPorts](https://www.macports.org). |

## Arch Linux (*common* only)

| Compiler | Status | Command | Date | Tested by | Comment |
| :---: | :---: | :---: | :---: | :---: | :--- |
| GCC 8.1.1 | ![OK](https://placehold.it/15/00ff00/000000?text=+) | `spack install icon-devel` | 2018-08-16 | Ralf Mueller (@m300064) |  |