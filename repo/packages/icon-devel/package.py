from spack import *


class IconDevel(Package):
    """ICON development environment."""

    homepage = 'https://code.mpimet.mpg.de/projects/iconpublic'
    git = 'git@git.mpimet.mpg.de:icon.git'

    version('develop', branch='master', submodules=True)

    depends_on('python', type='build')
    depends_on('perl', type='build')

    depends_on('netcdf-fortran')
    depends_on('blas')
    depends_on('lapack')
    depends_on('mpi')

    depends_on('libmtime')
    depends_on('libself@0.1')
    depends_on('libcdi@1.7.0:1.7.999+fortran')
    depends_on('libsct')
    depends_on('yac')

    def install(self, spec, prefix):
        success_file = open(join_path(prefix, 'success_flag.txt'), 'w')
        success_file.close()

