from spack import *


class Icon(AutotoolsPackage):
    """Icosahedral Nonhydrostatic Weather and Climate Model"""

    homepage = 'https://code.mpimet.mpg.de/projects/iconpublic'
    git = 'git@git.mpimet.mpg.de:icon-cimd.git'

    version('2.1.0', branch='icon-cimd/icon-cimd-new-configure')

    variant('mpi', default=True,
            description='Enable MPI (parallelization) support')

    patch('overlong_filename.patch')

    depends_on('python', type='build')
    depends_on('perl', type='build')

    depends_on('netcdf-fortran')
    depends_on('blas')
    depends_on('lapack')
    depends_on('mpi', when='+mpi')

    depends_on('libmtime')
    depends_on('libself@0.1')
    depends_on('libcdi@1.7.0:1.7.999+fortran')

    def configure_args(self):
        libs = self.spec['libmtime'].libs + \
               self.spec['libself'].libs + \
               self.spec['lapack:fortran'].libs + \
               self.spec['blas:fortran'].libs + \
               self.spec['netcdf-fortran'].libs + \
               self.spec['libcdi:fortran'].libs
        args = ['LIBS=' + libs.link_flags,
                'PYTHON=' + self.spec['python'].command.path,
                # Omit optimizations for now
                'FCFLAGS=-g']

        if self.spec.satisfies('+mpi'):
            args.extend(['--enable-mpi', 'FC=' + self.spec['mpi'].mpifc])
        else:
            args.append('--disable-mpi')
        return args
