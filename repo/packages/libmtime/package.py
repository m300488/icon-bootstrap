from spack import *
import os


class Libmtime(AutotoolsPackage):
    """Time management library. This project aims to provide time, calendar,
    and event handling for model applications."""

    homepage = 'https://code.mpimet.mpg.de/projects/mtime'
    git = 'git@git.mpimet.mpg.de:libmtime.git'

    version('develop', branch='develop')

    version('1.0.8', tag='1.0.8')
    version('1.0.7', tag='1.0.7')
    version('1.0.6', tag='1.0.6')

    variant('examples', default=True, description='Enable examples')

    depends_on('autoconf', type='build')
    depends_on('automake', type='build')
    depends_on('libtool', type='build')

    patch('simulate_iau.patch', when='@1.0.7')
    patch('example_option_develop.patch', when='@develop')
    patch('example_option_master.patch', when='@:1.0.8')

    force_autoreconf = True

    # The library fails to build in parallel
    parallel = False

    def configure_args(self):
        return self.enable_or_disable('examples')

    # Intel compiler complains that it cannot find certain modules.
    @run_after('install')
    def install_missing_mods(self):
        # First, we need to get the extension of the Fortran module files.
        # The module mtime_hl seems to be install correctly.
        existing_mod = 'mtime_hl'

        installed_mod = find(self.prefix.include,
                             existing_mod + '.*',
                             recursive=False)

        if not installed_mod:
            # Maybe the compiler generates module files with names in uppercase
            installed_mod = find(self.prefix.include,
                                 existing_mod.upper() + '.*',
                                 recursive=False)

        if len(installed_mod) == 1:
            # We can be more or less sure that we have found what we wanted
            extension = os.path.basename(installed_mod[0])[len(existing_mod):]
            mod_files = find(join_path(self.stage.source_path, 'src'),
                             '*' + extension,
                             recursive=False)
            for path in mod_files:
                basename = os.path.basename(path)
                if not os.path.exists(
                        join_path(self.prefix.include, basename)):
                    install(path, self.prefix.include)
