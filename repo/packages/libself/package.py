from spack import *
import os
import fnmatch
import inspect


class Libself(AutotoolsPackage):
    """Self is a fortran library that provides maps and vectors for arbitrary
    typed items."""

    homepage = 'https://code.mpimet.mpg.de/projects/self-standard-extendible-library-for-fortran'
    git = 'git@git.mpimet.mpg.de:libself.git'

    version('develop', branch='master')
    version('0.2', branch='libself-icon_v0.2')
    version('0.1', branch='libself-icon_v0.1')

    depends_on('python', type='build')

    patch('install.patch', when='@:0.2')
    depends_on('autoconf', when='@:0.2', type='build')

    # Although automake is not used directly, it's required to get recent
    # version of aclocal, which is called by autoreconf.
    depends_on('automake', when='@:0.2', type='build')

    @when('@:0.2')
    def patch(self):
        # Old versions of GNU patch seem not to be able to rename files
        for dirpath, _, filenames in os.walk('src'):
            for filename in fnmatch.filter(filenames, '*.F90'):
                new_filename = os.path.splitext(filename)[0] + '.f90'
                os.rename(os.path.join(dirpath, filename),
                          os.path.join(dirpath, new_filename))

    @when('@:0.2')
    def autoreconf(self, spec, prefix):
        inspect.getmodule(self).autoreconf('-ivf')

    def configure_args(self):
        return ['PYTHON=' + self.spec['python'].command.path]
